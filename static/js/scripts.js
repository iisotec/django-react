// table user tracing twitter
// modificado

function datailt_themes_interest(keywork) {
    dt_detailt_t_interest = $('#table_detailt_interest').dataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        ajax: '/influenciador/detailt-themes-interest-ajax?keywork=' + keywork,
        language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},
        columns: [
            {data: "id"},
            {data: "text"},
            {data: "createRegister"},
            {data: "retweetCount"},
            {data: "favoriteCount"},
            {data: "userScreenName"},
            {data: "userName"},
            {data: "userFollowers"},
            {data: "userFriends"}
        ],
        columnDefs: [
            {
                targets: 0,
                render: function (data, type, row) {
                    return '<a href="https://twitter.com/' + row.userScreenName + '/status/' + row.id + '" target="_blank">Link twitter</a>';
                }
            },
            {
                targets: 1,
                data: null,
                orderable: false
            },
            {
                targets: 2,
                render: function (data, type, row) {
                    if (data == null || data == '') {
                        return '';
                    } else {
                        return data;
                    }
                }
            },
            {
                targets: 5,
                render: function (data, type, row) {
                    return '<a href="https://twitter.com/' + data + '" target="_blank">' + data + '</a>';
                }
            }
        ]
    });
    return dt_detailt_t_interest
}

// Temas interes Instagram
function datailt_themes_interest_instagram(keywork) {
    dt_detailt_t_interest = $('#table_detailt_interest_instagram').dataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        ajax: '/instagram/detailt-themes-interest-instagram-ajax?keywork=' + keywork,
        language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},
        columns: [
            {data: "id"},
            {data: "createRegister"},
            {data: "create"},
            {data: "createUpdate"},
            {data: "captionText"},
            {data: "url"},
            {data: "userUsername"},
            {data: "userFullName"}
        ],
        columnDefs: [
            {
                targets: 5,
                render: function (data, type, row) {
                    return '<a href="' + row.url + '" target="_blank">Ver en Instagram</a>';
                }
            },
            {
                targets: 1,
                data: null,
                orderable: false
            }
        ]
    });
    return dt_detailt_t_interest
}

$(document).ready(function () {
    var dt2 = $('#tableThemesInterest').DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: '/api2/themes-interest/',
                type: 'GET'
            },
            language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},

            columns: [
                {data: "i_id"},
                {data: "t_keyword"},
                {data: "d_created"},
                {data: "b_status"}
            ],
            columnDefs: [
                {
                    targets: 1,
                    render: function (data, type, row) {
                        //return  '<a class="action_status" title="Cambiar de estado"><input type="checkbox" class="a_DetailInterestModal" data-id="'+data+'" value="" '+data+'> </a>'
                        return ' <a class="a_DetailInterestModal" href="#" title="Ver detalles">' + data + '</a>';
                    },
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol, a) {
                        $('.a_DetailInterestModal', nTd).click(function (e) {
                            datailt_themes_interest(sData)
                            $('#DetailInterestModal').modal()
                            if (sData.substr(0, 1) === '#') {
                                $('.lbl_datilt_theme').text(sData)
                                sData = sData.substr(1, sData.length)
                                $('.btn_view_detailt_theme_tw').attr('href', 'https://twitter.com/hashtag/' + sData + '?src=tren')
                            } else {
                                $('.lbl_datilt_theme').text(sData)
                                $('.btn_view_detailt_theme_tw').attr('href', 'https://twitter.com/search?q=' + '"' + sData + '"' + '&src=tren')
                            }
                        });
                    }
                },
                {
                    targets: 3,
                    render: function (data, type, row) {
                        if (data == '1') {
                            return '<span class="badge badge-success"> <i class="fa fa-check"></i> </span>';
                        }
                        if (data == '0') {
                            return '<span class="badge badge-danger"> <i class="fa fa-times"></i> </span>';
                        } else {
                            return 'No definido';
                        }
                    }
                },
                {
                    targets: 4,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        var status_theme = ''
                        if (data.b_status == 1) {
                            status_theme = 'checked'
                        }
                        return '<span class="text-success data-toggle="tooltip" data-placement="left" title="Editar"></span> <a data-id="' + data.i_id + '" data-keyword="' + data.t_keyword + '" class="badge badge-info action_edit_theme_interest"><i class="fa fa-edit"></i></a>' + '&nbsp;' +
                            '<span class="text-alert data-toggle="tooltip" data-placement="left" title="Eliminar"></span> <a data-id="' + data.i_id + '" data-keyword="' + data.t_keyword + '" class="badge badge-danger action_delete_utheme_interest title="Eliminar"><i class="fa fa-trash"></i></a>' + '&nbsp;' +
                            '</span> <a class="action_status" title="Cambiar de estado"><input type="checkbox" class="check_status_theme" data-id="' + data.i_id + '" value="" ' + status_theme + '> </a>';
                    },
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol, a) {
                        $(".action_delete_utheme_interest", nTd).click(function (e) {
                            var id_user = $(this).attr("data-id")
                            $('#confirm').modal('toggle');
                            $('#btn_delete').on('click', function (e) {
                                $.ajax({
                                    data: {
                                        id: id_user,
                                        csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                                    },
                                    type: 'post',
                                    url: '/influenciador/delete-theme-interest-ajax',
                                    success: function (data) {
                                        var resultado = JSON.parse(data);
                                        if (resultado['estado']) {
                                            dt2.ajax.url('/api2/themes-interest/').load();
                                        }
                                    },
                                    error: function () {
                                        alert('error del servidor o opcion no válida..!');
                                    }
                                });
                            })

                        });
                        $(".action_edit_theme_interest", nTd).click(function (e) {
                            $('#editThemeInterestModal').modal('toggle');
                            $('#u_keywork_edit').val($(this).attr("data-keyword"))
                            $('.id_theme_interest_edit').val($(this).attr("data-id"))
                            $('.t_theme_interest_status_edit').val(true)
                        });
                        // detect event checked
                        $(".check_status_theme", nTd).click(function (e) {
                            status = $(this).is(':checked')
                            id_theme = $(this).attr("data-id")
                            $.ajax({
                                data: {
                                    id_theme: id_theme,
                                    status: status,
                                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                                },
                                type: 'post',
                                url: '/influenciador/update-status-theme_interest-ajax',
                                success: function (data) {
                                    var resultado = JSON.parse(data);
                                    if (resultado['estado']) {
                                        dt2.ajax.url('/api2/themes-interest/').load();
                                    } else {
                                        alert('error:' + resultado['error'])
                                    }
                                },
                                error: function () {
                                    alert('error del servidor o opcion no válida..!');
                                }
                            });
                        })


                    }

                },
            ],
        });

// table, themes ineterest
    var dt1 = $('#table_tracing_user').DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: '/api2/user-tracing/',
                type: 'GET'
            },
            language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},

            columns: [
                {data: "i_id"},
                {data: "v_user_screen_name"},
                {data: "v_user_country"},
                {data: "b_data_status_download"},
                {data: "d_datetime_register"},
                {data: "b_status"}
            ],
            columnDefs: [
                {
                    targets: 3,
                    render: function (data, type, row) {
                        if (data == '0') {
                            return 'Pendiente';
                        }
                        if (data == '1') {
                            return 'Siguiendo';
                        } else {
                            return 'No definido';
                        }
                    }
                },
                {
                    targets: 1,
                    render: function (data, type, row) {
                        return '<a href="https://twitter.com/' + data + '" target="_blank">' + "@" + data + '</a>';
                    }
                },
                {
                    targets: 5,
                    render: function (data, type, row) {
                        if (data == '1') {
                            return '<span class="badge badge-success"> <i class="fa fa-check"></i> </span>';
                        }
                        if (data == '0') {
                            return '<span class="badge badge-danger"> <i class="fa fa-times"></i> </span>';
                        } else {
                            tableThemesInterest
                            return 'No definido';
                        }
                    }
                },
                {
                    targets: 6,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        // para checked
                        var status = ''
                        if (data.b_status == 1) {
                            status = 'checked'
                        }
                        return '<span class="text-success data-toggle="tooltip" data-placement="left" title="Editar"></span> <a data-id="' + data.i_id + '" data-name="' + data.v_user_screen_name + '" data-country="' + data.country + '" class="badge badge-info action_edit_user"><i class="fa fa-edit"></i></a>' + '&nbsp;' +
                            '<span class="text-alert data-toggle="tooltip" data-placement="left" title="Eliminar"></span> <a data-id="' + data.i_id + '" data-name="' + data.v_user_screen_name + '" class="badge badge-danger action_delete_theme_interest" title="Eliminar"><i class="fa fa-trash"></i></a>' + '&nbsp;' +
                            '</span> <a class="action_status" title="Cambiar de estado"><input type="checkbox" class="check_status" data-id="' + data.i_id + '" value="' + data.b_status + '" ' + status + '> </a>';
                    },
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol, a) {
                        $(".action_delete_theme_interest", nTd).click(function (e) {
                            var id_user = $(this).attr("data-id")
                            $('#confirm').modal('toggle');
                            $('.sp_screen_name').text($(this).attr("data-name"))
                            $('#btn_delete').on('click', function (e) {
                                $.ajax({
                                    data: {
                                        id: id_user,
                                        csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                                    },
                                    type: 'post',
                                    url: '/influenciador/delete_data_tw_user_ajax',
                                    success: function (data) {
                                        var resultado = JSON.parse(data);
                                        if (resultado['estado']) {
                                            dt1.ajax.url('/api2/user-tracing/').load();
                                        }
                                    },
                                    error: function () {
                                        alert('error del servidor o opcion no válida..!');
                                    }
                                });
                            })

                        });
                        $(".action_edit_user", nTd).click(function (e) {
                            $('#editUserTracingModal').modal('toggle');
                            $('#u_screen_name_edit').val($(this).attr("data-name"))
                            $("#u_country_edit option[value=" + $(this).attr("data-country") + "]").prop('selected', true);
                            $('.id_user_edit').val($(this).attr("data-id"))
                            $('.t_user_status_edit').val(true)
                            $('.alert_user_exist_edit').hide()
                        });
                        // detect event checked
                        $(".check_status", nTd).click(function (e) {
                            status = $(this).is(':checked')
                            id_theme = $(this).attr("data-id")
                            $.ajax({
                                data: {
                                    id_theme: id_theme,
                                    status: status,
                                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                                },
                                type: 'post',
                                url: '/influenciador/update-status-tw-user-ajax',
                                success: function (data) {
                                    var resultado = JSON.parse(data);
                                    if (resultado['estado']) {
                                        dt1.ajax.url('/api2/user-tracing/').load();
                                    } else {
                                        alert('error:' + resultado['error'])
                                    }
                                },
                                error: function () {
                                    alert('error del servidor o opcion no válida..!');
                                }
                            });
                            // if($(this).is(':checked')) {
                            //    alert("HOLAAAA CHECK")
                            // }
                            // else{
                            //   alert("No hay CHECK")
                            // }
                        })
                    }
                },
            ],
        });
});

$('.btn_update_tw_user').on('click', function (e) {
    // $('#exampleModal').modal('toggle');
    // console.log($(".c_input_id_tw_user").val())
    if ($('#form_edit_twitter_user').valid()) {
        category_name = []
        $("#p_categories option:selected").each(function () {
            category_name.push(this.text);
        });
        $.ajax({
            data: {
                id_user: $('#p_id_user').text(),
                country: $("#s_country option:selected").val(),
                age: $('#ip_age').val(),
                gender: $('#s_gender').val(),
                autority: $('#p_autority').val(),
                category_id: $('#p_categories').val(),
                category_name: category_name,
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            type: 'post',
            url: '/influenciador/update_tw_user_ajax',
            success: function (data) {
                var resultado = (data);
                if (resultado['estado']) {
                    alert("Se ha actualizado correctamente...!")
                    // $('#table_influenciador').DataTable().ajax.reload();
                    $('#table_influenciador').DataTable().draw('page');
                    $('#exampleModal').modal('toggle');
                } else {
                    alert("Error de actualizacin..!")
                }
            }
        });
    }
});

$('#u_screen_name').on('change', function () {

    $.ajax({
        data: {
            u_screen_name: $('#u_screen_name').val(),
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        type: 'post',
        url: '/influenciador/verify_acount_user',
        success: function (data) {
            var resultado = JSON.parse(data);
            if (resultado['estado']) {
                $('.t_user_status').val(true)
                $('.alert_user_exist').hide()
            } else {
                $('.t_user_status').val(false)
                $('.alert_user_exist').show()
            }
            // users exists
            if (resultado['estado_cuenta']) {
                $('.t_user_status_exists').val(true)
                $('.alert_user_exist_db').show()
            } else {
                $('.t_user_status_exists').val(false)
                $('.alert_user_exist_db').hide()
            }
        }
    });
})

$('#u_screen_name_edit').on('change', function () {

    $.ajax({
        data: {
            u_screen_name: $('#u_screen_name_edit').val(),
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        type: 'post',
        url: '/influenciador/verify_acount_user',
        success: function (data) {
            var resultado = JSON.parse(data);
            if (resultado['estado']) {
                $('.t_user_status_edit').val(true)
                $('.alert_user_exist_edit').hide()
            } else {
                $('.t_user_status_edit').val(false)
                $('.alert_user_exist_edit').show()
            }
        }
    });
})

$('.btn_save_user_tracing').on('click', function (e) {
    $(".cargar_loader_ajax").show();
    if ($("#form_acount_user_tracing").valid() && $('.t_user_status').val() == 'true' && $('.t_user_status_exists').val() == 'false') {
        $.ajax({
            data: {
                u_screen_name: $('#u_screen_name').val(),
                u_country: $("#u_country option:selected").val(),
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            type: 'post',
            url: '/influenciador/save_user_acount_tracing_ajax',
            success: function (data) {
                $(".cargar_loader_ajax").hide();
                var resultado = JSON.parse(data);
                if (resultado['estado']) {
                    $('#acountUserTracingModal').modal('toggle');
                    alert("Se ha guardado correctamente...!")
                    // $('#table_influenciador').DataTable().ajax.reload();
                    $('#table_tracing_user').DataTable().draw('page');
                    $('#form_acount_user_tracing')[0].reset();
                    $('.alert_user_exist').hide()
                    $('.alert_user_exist_db').hide()
                } else {
                    alert("Error de guardado..!")
                    $(".cargar_loader_ajax").hide();
                }
            },
            error: function () {
                alert('error del servidor o opcion no válida..!');
                $(".cargar_loader_ajax").hide();
            }
        });

    } else {
        $(".cargar_loader_ajax").hide();
    }
})

$('.btn_save_theme_interest').on('click', function (e) {
    $(".loader_theme_ajax").show();
    if ($("#form_theme_interest").valid()) {
        $.ajax({
            data: {
                u_keywork: $('#u_keywork').val(),
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            type: 'post',
            url: '/influenciador/save-theme-interest-ajax',
            success: function (data) {
                $(".loader_theme_ajax").hide();
                var resultado = JSON.parse(data);
                if (resultado['estado']) {
                    $('#acountTableThemesInterest').modal('toggle');
                    alert("Se ha guardado correctamente...!")
                    // $('#table_influenciador').DataTable().ajax.reload();
                    $('#tableThemesInterest').DataTable().draw('page');
                    $('#form_theme_interest')[0].reset();
                } else {
                    alert("Error de guardado..!" + resultado['error'])
                    $(".loader_theme_ajax").hide();
                }
            },
            error: function () {
                alert('error del servidor o opcion no válida..!');
                $(".loader_theme_ajax").hide();
            }
        });

    } else {
        $(".loader_theme_ajax").hide();
    }
})

$('.btn_update_user_tracing').on('click', function (e) {
    if ($("#form_acount_user_tracing_edit").valid() && $('.t_user_status_edit').val() == 'true') {
        $('.cargar_loader_edit_ajax').show()
        $.ajax({
            data: {
                id_user: $('.id_user_edit').val(),
                u_screen_name: $('#u_screen_name_edit').val(),
                u_country: $("#u_country_edit option:selected").val(),
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            type: 'post',
            url: '/influenciador/edit_data_tw_user_ajax',
            success: function (data) {
                $('.cargar_loader_edit_ajax').hide()
                var resultado = JSON.parse(data);
                if (resultado.estado) {
                    alert("Se ha actualizado correctamente..!")
                    $('#table_tracing_user').DataTable().draw('page');
                    $('#editUserTracingModal').modal('toggle');
                } else {

                    alert('Error al actualizar:' + resultado.error)
                }
            }
        });

    } else {
        $(".cargar_loader_ajax").hide();
    }
})

$('.btn_update_theme_interest').on('click', function (e) {
    if ($("#form_theme_interest_edit").valid() && $('.t_theme_interest_status_edit').val() == 'true') {
        $('.loader_edit_ajax').show()
        $.ajax({
            data: {
                i_id: $('.id_theme_interest_edit').val(),
                keywork: $('#u_keywork_edit').val(),
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            type: 'post',
            url: '/influenciador/edit-theme-interest-ajax',
            success: function (data) {
                $('.loader_edit_ajax').hide()
                var resultado = JSON.parse(data);
                if (resultado.estado) {
                    alert("Se ha actualizado correctamente..!")
                    $('#tableThemesInterest').DataTable().draw('page');
                    $('#editThemeInterestModal').modal('toggle');
                } else {

                    alert('Error al actualizar:' + resultado.error)
                }
            }
        });

    } else {
        $(".loader_edit_ajax").hide();
    }
})


$('.btn_add_user_tracing').on('click', function (e) {
    $('#acountUserTracingModal').modal()
    $('#form_acount_user_tracing')[0].reset();
    $('.alert_user_exist').hide()

})
$('.btn_add_theme_interest').on('click', function (e) {
    $('#acountTableThemesInterest').modal()
    $('#form_theme_interest')[0].reset();
})


// insert influenciadores
$('#register_new_influencer').click(function () {
    $('#addInfluencerModal').modal('toggle');
    $('#form_add_influencer #screen_name').val('');
})

// add user instagram
$('#register_new_user_instagram').click(function () {
    $('#addInUserInstagramModal').modal('toggle');
    $('#form_add_user_instagram #screen_name').val('');
})


$('#btnregistroinfluencer').click(function () {

    if ($("#form_add_influencer").valid()) {
        var form = $('#form_add_influencer');
        $(".loader_insert_influencer_ajax").show();
        $.ajax({
            type: "POST",
            url: '/influenciador/insert-influencer-ajax',
            data: form.serialize(),
            success: function (data) {
                $(".loader_insert_influencer_ajax").hide();
                var resultado = JSON.parse(data);
                if (resultado.estado) {
                    alert("Se ha actualizado correctamente...!")
                    $('#table_influenciador').DataTable().draw('page');
                    $('#addInfluencerModal').modal('toggle');
                }
            },
            error: function () {
                alert('Disculpa, error del servidor o opcion no válida..!');
                $(".loader_insert_influencer_ajax").hide();
            }

        });
    }

})

// INSTAGRAM
$(document).ready(function () {
    var dt = $('#table_user_instagram').DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            bDestroy: true,
            bJQueryUI: true,
            ajax: '/instagram/user_datatable_ajax?code_country=' + $("#s_country_val option:selected").val(),
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            columns: [
                {data: "id"},
                {data: "biography"},
                {data: "userName"},
                {data: "userFullName"},
                {data: "website"},
                {data: "createRegister"},
                {data: "isPrivate"},
                {data: "urlProfilePicture"},
                {data: "followersCount"},
                {data: "followingCount"},
                {data: "autority"},
                {data: "is_business_account"},
                {data: "country"}
            ],
            columnDefs: [
                {
                    targets: 4,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (data) {
                            return '<a target="_blank" href="' + data + '">Enlace sitio</a>';
                        } else
                            return ""
                    },
                },
                {
                    targets: 7,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        return '<a target="_blank" href="' + data + '">Enlace imagen</a>';
                    },
                },
                {
                    targets: 13,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (data.status_manual_tracking) {
                            return '<input type="checkbox" class="check_status_manual" data-id="' + data.id + '" value="" checked >';
                        } else {
                            return '<input type="checkbox" class="check_status_manual" data-id="' + data.id + '" value="" >';
                        }
                    },
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol, a) {
                        $(".check_status_manual", nTd).change(function (e) {
                            Swal({
                                title: '¿Estás seguro activar/desactivar seguimiento manual a esta cuenta?',
                                text: "Tener cuidado que no sea influenciador y que tenga identificado país",
                                type: 'question',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Si!'
                            }).then((result) => {
                                if (result.value) {
                                    status = $(this).is(':checked')
                                    id_user = $(this).attr("data-id")
                                    $.ajax({
                                        data: {
                                            id_user: id_user,
                                            status: status,
                                            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                                        },
                                        type: 'post',
                                        url: '/instagram/update-check-status-manual-ajax',
                                        success: function (data) {
                                            if (data['status']) {
                                                dt.ajax.url('/instagram/user_datatable_ajax?code_country=' + $("#s_country_val option:selected").val()).load();
                                            } else {
                                                alert('error:' + data['error'])
                                            }
                                        },
                                        error: function () {
                                            alert('error del servidor o opcion no válida..!');
                                        }
                                    });
                                } else {
                                    dt.ajax.url('/instagram/user_datatable_ajax?code_country=' + $("#s_country_val option:selected").val()).load();
                                }
                            })
                        });
                    }
                },
                {
                    targets: 14,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {

                        if (row['status_tacking_history'] === null || row['status_tacking_history'] === false) {
                            return `<input type="checkbox" data-id="${data.id}" data-name="${row.userName}" class="status_history" data-id="" value="">`;
                        } else {
                            return `<input type="checkbox" data-id="${data.id}" data-name="${row.userName}" class="status_history" data-id="" value="" checked>`;
                        }

                    },
                },
                {
                    targets: 15,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (row.manually_validated_data) {
                            return '<span class="text-success" data-toggle="tooltip" data-placement="left" ' +
                                'title="Validado"><i class="fa fa-check-circle "></i></span> <a data-id="' + row.id + '" ' +
                                'class="badge badge-primary" data-toggle="tooltip" data-name="'+row.userName+'" data-placement="left" title="Editar">' +
                                '<i class="fa fa-edit"></i></a>';
                        } else {
                            return '<span class="text-secondary" data-toggle="tooltip" data-placement="left" ' +
                                'title="Por validar" style="color:#b5b6b7!important"><i class="fa fa-circle "></i></span> ' +
                                '<a data-id="' + row.id + '" data-name="'+row.userName+'" class="badge badge-primary" data-toggle="tooltip" ' +
                                'data-placement="left" title="Editar"><i class="fa fa-edit"></i></a>';
                        }
                    },
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol, a) {
                        $("a", nTd).click(function (e) {
                            $.ajax({
                                data: {
                                    id_user: $(this).attr("data-id"),
                                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                                },
                                type: 'post',
                                url: '/instagram/detail_data_instagram_user_ajax',
                                success: function (data) {
                                    var resultado = JSON.parse(data);
                                    if (resultado.estado) {
                                        if (resultado.data.country) {
                                            $("#s_country option[value=" + resultado.data.country + "]").prop('selected', true);
                                        } else {
                                            $('#s_country').prop('selectedIndex', 0);
                                        }

                                        if (resultado.data.age) {
                                            $('#ip_age').val(resultado.data.age);
                                        } else {
                                            $('#ip_age').val(0);
                                        }

                                        if (resultado.data.gender) {
                                            $("#s_gender option[value=" + resultado.data.gender + "]").prop('selected', true);
                                        } else {
                                            $('#s_gender').prop('selectedIndex', 2);
                                        }
                                        // print dailt info user
                                        $('#p_id_user').text(resultado.data.id);
                                        $('#p_country').text(resultado.data.country);
                                        $('#p_screen_name').text(resultado.data.userName);
                                        $('#p_name').text(resultado.data.userFullName);
                                        $('#p_validate').text(resultado.data.manually_validated_data);
                                        $('#p_count_follower').text(resultado.data.followersCount);
                                        $('#p_count_friend').text(resultado.data.followingCount);
                                        // $('#p_autority').text(resultado.data.autority);
                                        $('#p_date_create_account').text(resultado.data.createRegister);
                                        $('#InstagramModal').modal('toggle');
                                    } else {
                                        alert("Error al optener datos..!")
                                    }
                                }
                            });
                        });
                    }
                },
            ],
        });
    $('#s_country_val').on('change', function (e) {
        dt.ajax.url('/instagram/user_datatable_ajax?code_country=' + $("#s_country_val option:selected").val()).load();
        // console.log(dt)
    });

});

$("#table_theme_interes_instagram").DataTable(
    {
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
        columnDefs: [
            {
                targets: 1,
                render: function (data, type, row) {
                    //return  '<a class="action_status" title="Cambiar de estado"><input type="checkbox" class="a_DetailInterestModal" data-id="'+data+'" value="" '+data+'> </a>'
                    return ' <a class="a_DetailInterestInstagramModal" href="#" title="Ver detalles">' + data + '</a>';
                },
                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol, a) {
                    $('.a_DetailInterestInstagramModal', nTd).click(function (e) {
                        datailt_themes_interest_instagram(sData)
                        $('#DetailInterestInstagramModal').modal()
                        $('.lbl_datilt_theme').text(sData)
                        $('.btn_view_detailt_theme_instagram').attr('href', 'https://www.instagram.com/explore/tags/' + sData)
                    });
                }
            },
        ],
    });


$('#btnregistro_usuario_instagram').click(function () {

    if ($("#form_add_user_instagram").valid()) {
        var form = $('#form_add_user_instagram');
        $(".loader_insert_user_ajax").show();
        $.ajax({
            type: "POST",
            url: '/instagram/insert-user-instagram-ajax',
            data: form.serialize(),
            success: function (data) {
                $(".loader_insert_user_ajax").hide();
                var resultado = JSON.parse(data);
                if (resultado.estado) {
                    alert("Se ha actualizado correctamente...!")
                    $('#table_user_instagram').DataTable().draw('page');
                    $('#addInUserInstagramModal').modal('toggle');
                } else {
                    alert("A acurrido un error: " + resultado.response)
                }
            },
            error: function () {
                alert('Disculpa, error del servidor o opcion no válida..!');
                $(".loader_insert_user_ajax").hide();
            }

        });
    }

})


// YOUTUBE
$(document).ready(function () {
    var dt1 = $('#table_youtube_channel').DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '/youtube/channel-datatables?code_country=' + $("#s_country_val option:selected").val(),
            language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},

            columns: [
                {data: "id"},
                {data: "createRegister"},
                {data: "createPublish"},
                {data: "title"},
                {data: "customUrl"},
                {data: "country"},
                {data: "viewCount"},
                {data: "commentCount"},
                {data: "subscriberCount"},
                {data: "videoCount"},
                {data: "urlChanel"},
                {data: "createUpdate"}
            ],
            columnDefs: [
                {
                    targets: 10,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (data) {
                            return '<a target="_blank" href="' + data + '">Visitar</a>';
                        } else
                            return ""
                    },
                },
                {
                    targets: 12,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        return '<span class="text-success data-toggle="tooltip" data-placement="left" title="Editar"></span> <a data-id="' + data.id + '" data-country="' + data.country + '" data-title="' + data.title + '" data-customurl="' + data.customUrl + '" data-urlchannel="' + data.urlChanel + '" class="badge badge-info action_edit_channel_youtube"><i class="fa fa-edit"></i></a>' + '&nbsp;' +
                            '<span class="text-alert data-toggle="tooltip" data-placement="left" title="Eliminar"></span> <a data-id="' + data.id + '" data-name="' + data.customUrl + '" class="badge badge-danger action_delete_theme_interest" title="Eliminar"><i class="fa fa-trash"></i></a>';
                    },
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol, a) {
                        $(".action_delete_theme_interest", nTd).click(function (e) {
                            var id_channel = $(this).attr("data-id")
                            $('#confirm').modal('toggle');
                            $('.sp_screen_name').text($(this).attr("data-name"))
                            $('#btn_delete').on('click', function (e) {
                                $.ajax({
                                    data: {
                                        id: id_channel,
                                        csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                                    },
                                    type: 'post',
                                    url: '/youtube/dalete-channel-ajax',
                                    success: function (data) {
                                        var resultado = JSON.parse(data);
                                        if (resultado['estado']) {
                                            dt1.ajax.url('/youtube/channel-datatables').load();
                                        }
                                    },
                                    error: function () {
                                        alert('error del servidor o opcion no válida..!');
                                    }
                                });
                            })

                        });
                        $(".action_edit_channel_youtube", nTd).click(function (e) {
                            $('#editChannelYoutube').modal('toggle');
                            $('#id_channel_edit').val($(this).attr("data-id"));
                            if ($(this).attr("data-country")) {
                                $("#s_country_youtube_edit option[value=" + $(this).attr("data-country") + "]").prop('selected', true);
                            } else {
                                $("#s_country_youtube_edit").val('');
                            }
                            $('#title').val($(this).attr("data-title"));
                            $('#customurl').val($(this).attr("data-customurl"));
                            $('#urlchannel').val($(this).attr("data-urlchannel"));
                            $('#t_channel_status_edit').val(true)
                            $('.alert_user_exist_edit').hide()
                        });
                    }
                },
            ],
        });
    $('#s_country_val').on('change', function (e) {
        dt1.ajax.url('/youtube/channel-datatables?code_country=' + $("#s_country_val option:selected").val()).load();
        // console.log(dt)
    });
});

$('#btn_update_channel_youtube').on('click', function (e) {
    if ($('#form_channel_youtube_edit').valid()) {
        $.ajax({
            data: {
                id: $('#id_channel_edit').val(),
                title: $('#title').val(),
                customurl: $('#customurl').val(),
                urlchanel: $('#urlchannel').val(),
                country: $('#s_country_youtube_edit').val(),
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            type: 'post',
            url: '/youtube/update-channel-ajax',
            success: function (data) {
                var resultado = (data);
                if (resultado['estado']) {

                    alert("Se ha actualizado correctamente...!")
                    // $('#table_influenciador').DataTable().ajax.reload();
                    $('#table_youtube_channel').DataTable().draw('page');
                    $('#editChannelYoutube').modal('toggle');
                } else {
                    alert("Error de actualizacin..!")
                }
            }
        });
    }
});

// VIDEO YOUTUBE

$(document).ready(function () {
    var dt1 = $('#table_youtube_video').DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '/youtube/video-datatables?code_country=' + $("#s_country_val option:selected").val(),
            language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},

            columns: [
                {data: 'id'},
                {data: 'createRegister'},
                {data: 'createPublish'},
                {data: 'title'},
                {data: 'description'},
                {data: 'country'},
                {data: 'url'},
                {data: 'channelId'},
                {data: 'channelTitle'},
                {data: 'viewCount'},
                {data: 'likeCount'},
                {data: 'dislikeCount'},
                {data: 'favoriteCount'},
                {data: 'commentCount'}
            ],
            columnDefs: [

                {
                    targets: 6,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (data) {
                            return '<a target="_blank" href="' + data + '">Visitar</a>';
                        } else
                            return ""
                    },
                },
            ],
        });
    $('#s_country_val').on('change', function (e) {
        dt1.ajax.url('/youtube/video-datatables?code_country=' + $("#s_country_val option:selected").val()).load();
        // console.log(dt)
    });
});

// Themes de Interest INSTAGRAM
$(".action_delete_utheme_interest_instagram").click(function (e) {
    var id_user = $(this).attr("data-id")
    $('#confirm').modal('toggle');
    $('#btn_delete').on('click', function (e) {
        $.ajax({
            data: {
                id: id_user,
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            type: 'post',
            url: '/instagram/delete-theme-interest-instagram-ajax',
            success: function (data) {
                var resultado = JSON.parse(data);
                if (resultado['estado']) {
                    location.reload();
                }
            },
            error: function () {
                alert('error del servidor o opcion no válida..!');
            }
        });
    })

});

$(".check_status_theme_instagram").click(function (e) {
    status = $(this).is(':checked')
    id_theme = $(this).attr("data-id")
    $.ajax({
        data: {
            id_theme: id_theme,
            status: status,
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        type: 'post',
        url: '/instagram/update-status-theme_interest-instagram-ajax',
        success: function (data) {
            var resultado = JSON.parse(data);
            if (resultado['estado']) {
                location.reload();
            } else {
                alert('error:' + resultado['error'])
            }
        },
        error: function () {
            alert('error del servidor o opcion no válida..!');
        }
    });
})

$('#btn_update_instagram_user').on('click', function (e) {
    // $('#exampleModal').modal('toggle');
    // console.log($(".c_input_id_tw_user").val())
    if ($('#form_edit_instagram_user').valid()) {
        $(".cargar_loader_ajax").show();
        $.ajax({
            data: {
                id_user: $('#p_id_user').text(),
                country: $("#s_country option:selected").val(),
                age: $('#ip_age').val(),
                gender: $('#s_gender').val(),
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            type: 'post',
            url: '/instagram/update_instagram_user_ajax',
            success: function (data) {
                var resultado = (data);
                if (resultado['estado']) {
                    $(".cargar_loader_ajax").hide();
                    alert("Se ha actualizado correctamente...!")
                    // $('#table_influenciador').DataTable().ajax.reload();
                    $('#table_user_instagram').DataTable().draw('page');
                    $('#InstagramModal').modal('toggle');

                } else {
                    alert("Error de actualizacin..!")
                }
            }
        });
    }
});

// add user instagram
$('#register_new_canal_youtube').click(function () {
    $('#addChannleYoutubeModal').modal('toggle');
    $('#form_add_channel_youtube #channel__name').val('');
    $('#form_add_channel_youtube #country').val('');
})

$('#btn_add_channel_youtube').click(function () {

    if ($("#form_add_channel_youtube").valid()) {
        var form = $('#form_add_channel_youtube');
        $(".loader_insert_user_ajax").show();
        $.ajax({
            type: "POST",
            url: '/youtube/insert-channel-youtube-ajax',
            data: form.serialize(),
            success: function (data) {
                $(".loader_insert_user_ajax").hide();
                var resultado = JSON.parse(data);
                if (resultado.estado) {
                    alert("Se ha insertado correctamente...!")
                    $('#table_youtube_channel').DataTable().draw('page');
                    $('#addChannleYoutubeModal').modal('toggle');
                } else {
                    alert("A acurrido un error youtube: " + resultado.error)
                }
            },
            error: function () {
                alert('Disculpa, error del servidor o opcion no válida..!');
                $(".loader_insert_user_ajax").hide();
            }

        });
    }

})

$(document).ready(function () {
    $(".p_categories").select2();
});
