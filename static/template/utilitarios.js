$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie !== '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) === (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }

        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }

});

function toggle_view_more(id_div_content, texto) {
    let showChar = 300;
    let ellipsestext = "...";
    let moretext = "Seguir leyendo >";
    if (texto.length > showChar) {
        let c = texto.substr(0, showChar);
        return `<div class="abstract_${id_div_content}">${c}${ellipsestext}</div>
                <div class="morecontent">${texto}</div>
                <p><span class="morelink" data-id="${id_div_content}">${moretext}</span></p>`;
    }
    else{
        return texto
    }
}

function click_view_more(selector_padre) {
    let moretext = "Seguir leyendo >";
    let lesstext = "Mostrar menos";
    $(selector_padre).on('click', '.morelink', function () {
        let id = $(this).attr('data-id');

        if ($(this).hasClass('less')) {
            $(this).removeClass('less');
            $(this).html(moretext);
            $(`.abstract_${id}`).removeClass('hidden');
        } else {
            $(this).addClass('less');
            $(this).html(lesstext);
            $(`.abstract_${id}`).addClass('hidden');
        }
        $(this).parent().prev().slideToggle('fast');
        $(this).prev().slideToggle('fast');
        return false;
    });
}