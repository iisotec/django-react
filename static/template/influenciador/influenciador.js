let country_val = $("#s_country_val option:selected").val();
let tipo_val = $("#s_tipo_val option:selected").val();
let dt = $('#table_influenciador');

$(document).ready(function () {
    get_listado_user();
    let table = dt.DataTable();

    $('#s_country_val').on('change', function (e) {
        let url_d = '/influenciador/datatable_ajax?code_country=' + $(this).val() + '&select_tipo=' + tipo_val;
        table.ajax.url(url_d).load();
    });
    $('#s_tipo_val').on('change', function (e) {
        let url_d = '/influenciador/datatable_ajax?code_country=' + country_val + '&select_tipo=' + $(this).val();
        table.ajax.url(url_d).load();
    });

});

function get_listado_user() {
    dt.DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            bDestroy: true,
            bJQueryUI: true,
            ajax: '/influenciador/datatable_ajax?code_country=' + country_val + '&select_tipo=' + tipo_val,
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            },
            columnDefs: [
                {
                    targets: 1,
                    render: function (data, type, row) {
                        return '<a href="https://twitter.com/' + data + '" target="_blank">' + "@" + data + '</a>';
                    }
                },
                {
                    targets: 5,
                    render: function (data, type, row) {
                        if (data === '1') return 'Masculino';
                        if (data === '0') return 'Femenino';
                        if (data === '2') return 'Empresa';
                        else return 'No definido';
                    }
                },
                {
                    targets: 13,
                    render: function (data, type, row) {
                        if (data == null) return 'Ninguno';
                        else return data;
                    }
                },
                {
                    targets: 14,
                    render: function (data, type, row) {
                        if (data == null || data === '') return 'Ninguno';
                        else return data;
                    }
                },
                {
                    targets: 15,
                    render: function (data, type, row) {
                        if (data == null || data === '') return 'Ninguno';
                        else return data;
                    }
                },
                {
                    targets: 16,
                    render: function (data, type, row) {
                        if (data == null || data === '') return 'Ninguno';
                        else return data;
                    }
                },
                {
                    targets: 17,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (row.manually_validated_data) {
                            return `<span class="text-success" data-toggle="tooltip" data-placement="left" 
                                    title="Validado"><i class="fa fa-check-circle "></i></span> <a data-id="${row.id}" 
                                    class="badge badge-primary" data-toggle="tooltip" data-placement="left" 
                                    title="Editar"> <i class="fa fa-edit"></i></a>`;
                        } else {
                            return `<span class="text-secondary" data-toggle="tooltip" data-placement="left" 
                                    title="Por validar" style="color:#b5b6b7!important">
                                    <i class="fa fa-circle"></i> </span> <a data-id="${row.id}" 
                                    class="badge badge-primary" data-toggle="tooltip" data-placement="left" 
                                    title="Editar"><i class="fa fa-edit"></i></a>`;
                        }
                    },
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol, a) {
                        let csr = $('input[name=csrfmiddlewaretoken]').val();
                        $("a", nTd).click(function (e) {
                            $.ajax({
                                data: {
                                    id_user: $(this).attr("data-id"),
                                    csrfmiddlewaretoken: csr
                                },
                                type: 'post',
                                url: '/influenciador/detail_data_tw_user_ajax',
                                success: function (data) {
                                    var resultado = JSON.parse(data);
                                    if (resultado.estado) {
                                        if (resultado.data.country) {
                                            $("#s_country option[value=" + resultado.data.country + "]").prop('selected', true);
                                        } else {
                                            $('#s_country').prop('selectedIndex', 0);
                                        }

                                        if (resultado.data.age) {
                                            $('#ip_age').val(resultado.data.age);
                                        } else {
                                            $('#ip_age').val(0);
                                        }

                                        if (resultado.data.gender) {
                                            $("#s_gender option[value=" + resultado.data.gender + "]").prop('selected', true);
                                        } else {
                                            $('#s_gender').prop('selectedIndex', 2);
                                        }
                                        // print dailt info user
                                        $('#p_id_user').text(resultado.data.id);
                                        $('#p_screen_name').text(resultado.data.screen_name);
                                        $('#p_name').text(resultado.data.name);
                                        $('#p_lang').text(resultado.data.lang);
                                        $('#p_time_zone').text(resultado.data.time_zone);
                                        $('#p_validate').text(resultado.data.validate);
                                        $('#p_location').text(resultado.data.location);
                                        $('#p_count_follower').text(resultado.data.count_follower);
                                        $('#p_count_friend').text(resultado.data.count_friend);
                                        $('#p_count_likes').text(resultado.data.count_likes);
                                        $('#p_count_listed').text(resultado.data.irank);
                                        $('#p_count_tweets').text(resultado.data.count_tweets);
                                        // $('#p_autority').text(resultado.data.autority);
                                        $('#p_autority').val(resultado.data.autority);
                                        $('#p_date_create_account').text(resultado.data.date_create_account);
                                        if (resultado.data.category_id) {
                                            $('.p_categories').select2('val', [resultado.data.category_id]);
                                        } else {
                                            $("#p_categories").val(null).trigger('change')
                                        }
                                        $('#exampleModal').modal('toggle');
                                    } else {
                                        alert("Error al optener datos..!")
                                    }
                                }
                            });
                        });
                    }
                }
            ],
            columns: [
                {data: "id"},
                {data: "screen_name"},
                {data: "name"},
                {data: "validate"},
                {data: "age"},
                {data: "gender"},
                {data: "location"},
                {data: "count_follower"},
                {data: "count_friend"},
                {data: "count_likes"},
                {data: "count_listed"},
                {data: "count_tweets"},
                {data: "autority"},
                {data: "weighted_autority"},
                {data: "weighted_follower"},
                {data: "weighted_interactions"},
                {data: "irank"}
            ]
        });
}