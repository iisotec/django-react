let table_websiter = $('#table_content_websites');
let url_content_site = table_websiter.attr('data-list-site');
let data_country = $("#s_name_pais option:selected");
let data_domain = $("#s_site_domain option:selected");

// websites
$(document).ready(function () {
    let vuelta = 1;

    let country = data_country.val();
    let domain = data_domain.val();

    table_websiter.DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            bDestroy: true,
            bJQueryUI: true,
            ajax: {
                url: url_content_site,
                type: 'GET',
            },
            language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},
            columns: [
                {data: 'status_approval'},
                {data: 'url'},
                {data: 'netloc'},
                {data: 'title_text'},
                {data: 'summary'},
                {data: 'text'},
                {data: 'authors'},
                {data: 'top_image'},
                {data: 'date_create'},
                {data: 'date_publication'},
                {data: 'country'},
                {data: 'domain'}
            ],
            columnDefs: [
                {orderable: false, targets: 0},
                {
                    targets: 0,
                    render: function (data, type, row) {
                        if (vuelta === 1) $('#allCheck').prop('checked', true);
                        let checked = data === true ? 'checked' : '';
                        if (data === true) $('#allCheck').prop('checked', true);
                        else {
                            $('#allCheck').prop('checked', false);
                            vuelta = 0
                        }
                        return `<div class="form-check">
                                <input type="checkbox" data-id="${row.id}" class="form-check-input check_row" name="check_${row.id}" ${checked}>
                                </div>`
                    }
                },
                {
                    targets: 1,
                    render: function (data, type, row) {
                        return `<a href="${data}" target="_blank">${data}</a>`;
                    }
                },
                {
                    targets: 5,
                    render: function (data, type, row) {
                        return toggle_view_more(row['id'], data);
                    }
                },
                {
                    targets: 7,
                    render: function (data, type, row) {
                        return '<a href="' + data + '" target="_blank"><img src="' + data + '" alt="" height="100" width="100"/></a>';
                    }
                },
                {
                    targets: 11,
                    render: function (data, type, row) {
                        return '<a href="http://' + data + '" target="_blank">' + data + '</a>';
                    }
                },
            ]
        });

    $('.container-fluid ').on('click', 'input[type="search"]', function () {
        vuelta = 1
    });

    let dt1 = table_websiter.DataTable();

    $('#s_name_pais').on('change', function (e) {
        domain = data_domain.val() === '' ? null : data_domain.val();
        country = $(this).val() === '' ? null : $(this).val();
        let url = url_content_site + '/' + country + '/' + domain + '/';
        dt1.ajax.url(url).load();
    });
    $('#s_site_domain').on('change', function (e) {
        domain = $(this).val() === '' ? 'null' : $(this).val();
        country = data_country.val() === '' ? 'null' : data_country.val();
        let url = url_content_site + '/' + country + '/' + domain + '/';
        dt1.ajax.url(url).load();
    });

    click_view_more('#table_content_websites');
});

function reload_table_for_filter() {
    let country = data_country.val();
    let domain = data_domain.val();
    let dt1 = table_websiter.DataTable();
    let url = url_content_site + '/' + country + '/ ' + domain + '/';
    dt1.ajax.url(url).load();
}

table_websiter.on('click', '.check_row', function () {
    let status = $(this).is(':checked');

    if (status === false) $('#allCheck').prop('checked', false);
    else {
        let var_1 = true;
        let var_2 = true;

        $('#table_content_websites tbody tr').each(function () {
            let input = $(this).find("td").find("div").find("input");
            if (input.is(':checked') === false) var_2 = false
        });

        if (var_1 === var_2) $('#allCheck').prop('checked', true);
    }

    let id_line = $(this).attr('data-id');
    change_status_for_line(status, id_line);
})

table_websiter.on('click', '#allCheck', function () {
    let statud_check = $(this).is(':checked');

    $('#table_content_websites tbody tr').each(function () {
        let input = $(this).find("td").find("div").find("input");

        if (statud_check === true) $(input).prop('checked', true);
        else $(input).prop('checked', false);

        let id_line = $(input).attr('data-id');
        let status = $(input).is(':checked')

        change_status_for_line(status, id_line)

    });

});

function change_status_for_line(status, id_line) {
    let csr = $('input[name=csrfmiddlewaretoken]').val();
    $.ajax({
        data: {
            id: id_line,
            status_approval: status,
            csrfmiddlewaretoken: csr
        },
        type: 'post',
        url: '/websites/update-status-content-site',
        success: function (data) {
            reload_table_for_filter();
        },
        error: function () {
            alert('error del servidor o opcion no válida..!');
        }
    });
}