let table_websiter = $('#table_websites');
let url_list_site = table_websiter.attr('data-list-site');

$('#register_new_website').click(function () {
    $('#form_website')[0].reset();
    $('.loader_insert_site_ajax').hide();
    $('#activat_ex_content').html('')
})

$(document).ready(function () {
    let table_websiter = $('#table_websites');
    let country = $("#s_country_val option:selected").val();
    let domain = $("#s_site_domain_val option:selected").val();
    let url = `${url_list_site}?code_country=${country}&select_domain=${domain}`;
    let vuelta = 1;

    let dt = table_websiter.DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            bDestroy: true,
            bJQueryUI: true,
            ajax: url,
            language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},
            columns: [
                {data: 'date_publication_available'},
                {data: 'website_last_scraping'},
                {data: 'website_url'},
                {data: 'website_host'},
                {data: 'website_redirect'},
                {data: 'website_register'},
                {data: 'status_site'},
                {data: 'country_code'},
                {data: 'time_zone'},
                {data: 'status_tracking'},
                {data: 'status_analysis'},
                {data: 'time_scraping'},
            ],
            columnDefs: [
                {orderable: false, targets: 0},
                {width: "5%", className: "text-center", targets: [1, 5]},
                {width: "1%", className: "text-center", targets: [0, 11, 10, 9, 8, 7, 6]},
                {width: "7%", className: "text-center", targets: [12]},
                {
                    targets: 12,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {

                        let url_detail_view = $('#table_websites').attr('data-url-deatil').slice(0, -2)
                        var status_site = data.status_site === 1 ? 'checked' : '';

                        return `<span class="text-success" data-toggle="tooltip" data-placement="left" title="Redirigir">
                                </span><a href="${url_detail_view}${data._id}" title ="Redirigir" class="edit_url badge badge-info" ><i class="fa fa-edit"></i></a>
                                <span class="text-alert" data-toggle="tooltip" data-placement="left" title="Eliminar"></span>
                                <a data-id="${data._id}" class="badge badge-danger action_delete_site" title="Eliminar">
                                <i class="fa fa-trash"></i></a> <a class="action_status" title="Cambiar estado del sitio">
                                <input type="checkbox" class="check_status_site" data-id="${data._id}" value="" ${status_site}></a>
                                <a class="badge badge-info action_restore_site" data-id_restore="${data._id}"
                                title="Restablecer scrapeo"><i class="fa fa-refresh"></i></a>`;
                    },

                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol, a) {
                        $(".edit_url", nTd).click(function (e) {
                            $(".se-pre-con").show();
                            $(".site-content").fadeOut();
                        });

                        $(".action_delete_site", nTd).click(function (e) {
                            var id_site = $(this).attr("data-id")
                            $('#confirm').modal('toggle');
                            $('#btn_delete').on('click', function (e) {
                                $.ajax({
                                    data: {
                                        id: id_site,
                                        csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                                    },
                                    type: 'post',
                                    url: '/websites/delete-site-ajax',
                                    success: function (data) {
                                        var resultado = JSON.parse(data);
                                        if (resultado['estado']) reload_table_for_filter()
                                    },
                                    error: function () {
                                        alert('error del servidor o opcion no válida..!');
                                    }
                                });
                            })

                        });

                        $(".check_status_site", nTd).click(function (e) {
                            let id_site = $(this).attr("data-id")
                            let status_site = $(this).is(':checked') === true ? 1 : 0
                            let csr = $('input[name=csrfmiddlewaretoken]').val()

                            $.ajax({
                                data: {
                                    id_site: id_site,
                                    status: status_site,
                                    csrfmiddlewaretoken: csr
                                },
                                type: 'post',
                                url: '/websites/update-status-site-ajax',
                                success: function (data) {
                                    // reload_table_for_filter();
                                    let resultado = JSON.parse(data);
                                    if (resultado['estado']) dt.ajax.reload(null, false)
                                    else alert('error:' + resultado['error'])
                                },
                                error: function () {
                                    alert('error del servidor o opcion no válida..!');
                                }
                            });
                        })


                    }
                },
                {
                    targets: 0,
                    render: function (data, type, row) {
                        if (vuelta === 1) $('#allCheck').prop('checked', true);
                        let check_val = row.date_publication_available === 1 ? 'checked' : ''
                        let val_bool = row.date_publication_available === 1

                        if (val_bool === true) $('#allCheck').prop('checked', true);
                        else {
                            $('#allCheck').prop('checked', false);
                            vuelta = 0
                        }

                        return `<div class="form-check"><input type="checkbox" data-id="${row._id}" class="form-check-input check_row" name="check_${row._id}" ${check_val}></div>`
                    }
                },
                {
                    targets: 2,
                    render: function (data, type, row) {
                        return `<div class="pl-0 pr-0 col-sm-1"><a class="" >${data}</a></div>`;
                    }
                },
                {
                    targets: 3,
                    render: function (data, type, row) {
                        return `<div class="pl-0 pr-0 col-sm-1"><a class="" >${data}</a></div>`;
                    }
                },
                {
                    targets: 4,
                    render: function (data, type, row) {
                        return `<div class="pl-0 pr-0 col-sm-1"><a href="${data}" target="_blank">${data}</a></div>`;
                    }
                },
                {
                    targets: 6,
                    render: function (data, type, row) {
                        if (data === 1) return `<span class="badge badge-success"> <i class="fa fa-check"></i> </span>`;
                        if (data === 0) return `<span class="badge badge-danger"> <i class="fa fa-times"></i> </span>`;
                        else return 'No definido';
                    }
                },
                {
                    targets: 9,
                    render: function (data, type, row) {
                        if (data === 1) return `<span class="badge badge-success"> <i class="fa fa-check"></i> </span>`;
                        if (data === 0) return `<span class="badge badge-danger"> <i class="fa fa-times"></i> </span>`;
                        else return 'No definido';
                    }
                },
                {
                    targets: 10,
                    render: function (data, type, row) {
                        if (data === 1) return `<span class="badge badge-success"> <i class="fa fa-check"></i> </span>`;
                        if (data === 0) return `<span class="badge badge-danger"> <i class="fa fa-times"></i> </span>`;
                        else return 'No definido';
                    }
                },
            ]
        });

    $('.container-fluid ').on('click', 'input[type="search"]', function () {
        vuelta = 1
    });

    $('#s_country_val').on('change', function (e) {
        reload_table_for_filter();
    });
    $('#s_site_domain_val').on('change', function (e) {
        reload_table_for_filter();
    });

});

// Acción restore scraping sitere site
$(document).on('click', '.action_restore_site', function (e) {
    let id_site = $(this).attr('data-id_restore')
    $('#btn_restore').attr('data-id', id_site);
    $('#confirm_restore_scraping').modal('toggle');
})

$('#btn_restore').on('click', function (e) {
    let id_site = $(this).attr('data-id');
    let csr = $('input[name=csrfmiddlewaretoken]').val()
    $.ajax({
        data: {
            id_site: id_site,
            csrfmiddlewaretoken: csr
        },
        type: 'post',
        url: '/websites/restore-status-site-ajax',
        success: function (data) {
            var resultado = JSON.parse(data);
            if (resultado['status']) {
                alert(resultado['response'])
                table_websiter.DataTable().draw('page');
            } else {
                alert('error:' + resultado['response'])
            }
        },
        error: function () {
            alert('error del servidor o opcion no válida..!');
        }
    });
})

$('#btn_refresh_all_analysis').on('click', function (e) {
    Swal({
        title: '¿Estás seguro de reastablecer en estado análisis a todos los sitios?',
        text: "¡No podrás revertir esto!",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!'
    }).then((result) => {
        if (result.value) {
            url = '/websites/refresh-all-analysis-ajax'
            $.ajax({
                type: 'post',
                url: url,
                success: function (data) {
                    var resultado = JSON.parse(data);

                    if (resultado['status']) {
                        swal(
                            'Se ha restablecido ok',
                            'ok',
                            'success'
                        )
                    } else {
                        swal(
                            'Error en restablecer los sitios!',
                            'Disculpa, ocurrio un problema interno',
                            'error'
                        )
                    }
                },
                error: function () {
                    swal(
                        'No se pudo restablecer!',
                        'Ocurrio un problema',
                        'error'
                    )
                }
            });
        }
    })
});

$('#check_delete_tag').on('click', function (e) {
    let tipo = $('#btnregistersite').data('type');
    if (tipo === 'Editar') {
        let id_site = $("#id_site").val();
        let status_check_tag = $(this).is(':checked') === 'true' ? 1 : 0;
        let csr = $('input[name=csrfmiddlewaretoken]').val()

        $.ajax({
            data: {
                id_site: id_site,
                status: status_check_tag,
                csrfmiddlewaretoken: csr
            },
            type: 'post',
            url: '/websites/update-remove-tag-content-ajax',
            success: function (data) {
                var resultado = JSON.parse(data);
                if (resultado['status']) {
                    swal(
                        'ok',
                        'Se activado la eliminacion de TAGS en contenido',
                        'success'
                    )
                    reload_table_for_filter();
                } else {
                    swal(
                        'Error en activar eliminacion de tags!',
                        'Disculpa, ocurrio un problema interno',
                        'error'
                    )
                }
            },
            error: function () {
                alert('error del servidor o opcion no válida..!');
            }
        });
    }
});

/*para listar PROVINCIAS al form de registro para filtral su cole*/
$('#country').on('change', function () {
    let type_select_time = $('#time_zone')
    post_country(this, type_select_time)
});

table_websiter.on('click', '#allCheck', function () {
    let statud_check = $(this).is(':checked')

    $('#table_websites tbody tr').each(function () {
        let input = $(this).find("td").find("div").find("input");

        if (statud_check === true) $(input).prop('checked', true);
        else $(input).prop('checked', false);
    });
});

$('#select-option').on('change', function () {
    let val_select = $(this).val()
    let array_id_site = [];

    Swal({
        title: '¿Estas seguro de cambiar el dato de la publicacion',
        text: "¡No podrás revertir esto!",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!'
    }).then((result) => {
        if (result.value) {
            $('#table_websites tbody tr').each(function () {
                let input = $(this).find("td").find("div").find("input");
                let id_site = $(input).data('id')

                if (val_select === 'desactivar_fecha' && input.is(':checked') === false) {
                    array_id_site.push(id_site)
                    change_status_date(array_id_site, 0)
                }
                if (val_select === 'activar_fecha' && input.is(':checked') === true) {
                    array_id_site.push(id_site)
                    change_status_date(array_id_site, 1)
                }
            });
        } else $("#select-option").val('')
    })
})

table_websiter.on('click', '.check_row', function () {
    let status = $(this).is(':checked');

    if (status === false) $('#allCheck').prop('checked', false);
    else {
        let var_1 = true;
        let var_2 = true;

        $('#table_websites tbody tr').each(function () {
            let input = $(this).find("td").find("div").find("input");
            if (input.is(':checked') === false) var_2 = false
        });

        if (var_1 === var_2) $('#allCheck').prop('checked', true);
    }
})

function post_country(object, type_select_time) {
    let csr = $('input[name=csrfmiddlewaretoken]').val();
    if ($(object).val()) {
        $.ajax({
            data: {
                'country_code': $(object).val(),
                csrfmiddlewaretoken: csr
            },
            type: 'post',
            url: '/websites/load-time-zone-ajax',
            success: function (data) {
                let objetos = JSON.parse(data);
                type_select_time.val(objetos['data']);
            },
            error: function () {
                alert('error del servidor o opcion no válida..!');
            }
        });
    }
}

function change_status_date(array_id_site, status) {
    let csr = $('input[name=csrfmiddlewaretoken]').val()
    $.ajax({
        data: {
            array_id_site: array_id_site,
            status: status,
            csrfmiddlewaretoken: csr
        },
        type: 'post',
        url: '/websites/update-status-date',
        success: function (data) {
            reload_table_for_filter()
            $("#select-option").val('')
        },
        error: function () {
            alert('error del servidor o opcion no válida..!');
        }
    });
}

function reload_table_for_filter() {
    let country = $("#s_country_val option:selected").val();
    let domain = $("#s_site_domain_val option:selected").val();
    let dt1 = table_websiter.DataTable();
    let url = `/websites/list-sites-ajax?code_country=${country}&select_domain=${domain}`;
    dt1.ajax.url(url).load();
}