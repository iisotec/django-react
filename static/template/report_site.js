let id_report = $('#section-report');
let input_initial = $('#data_initial');
let input_end = $('#data_end');
let total = 0;
let csr = $('input[name=csrfmiddlewaretoken]').val();

$(document).ready(function () {
    get_report_initial(0);

    $('#enviar_report').on('click', function () {
        $('#pagination-demo').html('');
        get_report_initial(0)
    });

    inicializar_input_date();

});

function inicializar_input_date() {
    let boot_iso = $('.bootstrap-iso form');
    let container = boot_iso.length > 0 ? boot_iso.parent() : "body";
    let options = {
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
    };

    input_initial.datepicker(options);
    input_end.datepicker(options);
}

function get_report_initial(page_initial) {
    let path_ing = $('#section-report').attr('data-path-img');
    $('#img_reporte').attr('src', path_ing + '/reload.svg');
    $.ajax({
        type: 'POST',
        data: {
            'data_initial': input_initial.val(),
            'time_initial': $('#time_initial').val(),
            'data_end': input_end.val(),
            'time_end': $('#time_end').val(),
            'fila_uno': page_initial,
            'csrfmiddlewaretoken': csr
        },
        url: id_report.attr('data-report'),
        success: function (data) {
            if (data['img'] !== 'No trae datos') {
                let d = new Date();
                $('#img_reporte').attr('src', path_ing + '/' + data['img'] + '?' + d.getTime());

                creacion_paginado(data)
            } else {
                $('#img_reporte').attr('src', path_ing + '/no_data.svg');
            }

        },
        error: function (error) {
            $('#img_reporte').attr('src', path_ing + '/no_data.svg')
        }
    });
}

function creacion_paginado(data) {
    let total = data['total'];
    let rows = data['rows'];
    let paginas = Math.ceil(total / rows);

    $('#pagination-demo').twbsPagination({
        totalPages: paginas,
        visiblePages: 5,
        next: 'Siguiente',
        prev: 'Antes',
        onPageClick: function (event, page) {
            get_report_initial(page - 1)
        }
    });
}