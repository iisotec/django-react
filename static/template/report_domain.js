let id_report = $('#section-report');
let input_domain = $('#domain');
let total = 0;
let csr = $('input[name=csrfmiddlewaretoken]').val();
let path_ing = $('#section-report').attr('data-path-img');

$(document).ready(function () {

    $('#img_reporte').attr('src', path_ing + '/no_data.svg');

    input_domain.on('change', function () {
        let dom = $(this).val();
        $('#img_reporte').attr('src', path_ing + '/reload.svg');
        $.ajax({
            type: 'POST',
            data: {
                'domain': dom,
                'csrfmiddlewaretoken': csr
            },
            url: id_report.attr('data-report-domain'),
            success: function (data) {
                if (data['img'] !== 'No trae datos') {
                    let d = new Date();
                    $('#img_reporte').attr('src', path_ing + '/' + data['img'] + '?' + d.getTime());
                } else {
                    $('#img_reporte').attr('src', path_ing + '/no_data.svg');
                }

            },
            error: function (error) {
                $('#img_reporte').attr('src', path_ing + '/no_data.svg');
            }
        });

    })

});
