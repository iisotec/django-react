let dt2 = $('#table_pendent_pages');


function dt_datailt_page_related(page_related_link) {
    console.log('dt_datailt_page_related--->'+page_related_link)
    dt_detailt_t_interest = $('#table_detailt_page_related').dataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        ajax: '/facebook/detailt-page-related-ajax?page_related_link=' + page_related_link,
        language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},
        columns: [
            {data: "id"},
            {data: "page_link"},
            {data: "page_name"},
            {data: "page_related_type_relation"},
            {data: "page_category_name"},
            {data: "page_username"},
            {data: "page_date_register_update"},
            {data: "page_related_link"},
            {data: "page_related_name"},
            {data: "page_related_username"}
        ],
        columnDefs: [
            {
                targets: 1,
                render: function (data, type, row) {
                    return '<a target="_blank" href="' + data + '">'+data+'</a>';
                }
            }
        ]
    });
    return dt_detailt_t_interest
}


$(document).ready(function () {
    let vuelta = 1;
    dt2.DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            bDestroy: true,
            bJQueryUI: true,
            ajax: {
                url: '/facebook/ajax_pendent_follow_pages_list/',
                type: 'GET'
            },
            language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},

            columns: [
                {data: "status_download"},
                {data: "id"},
                {data: "url_page"},
                {data: "slug"},
                {data: "name_page"},
                {data: "create_update_register"},
                {data: "count_related_like"}
            ],
            columnDefs: [
                {width: "2%", className: "text-center", targets: [0]},
                {orderable: false, targets: 0},
                {
                    targets: 0,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (vuelta === 1) $('#allCheck').prop('checked', true);
                        let checked = data === true ? 'checked' : '';
                        let val_bool = data === true

                        if (val_bool === true) $('#allCheck').prop('checked', true);
                        else {
                            $('#allCheck').prop('checked', false);
                            vuelta = 0
                        }
                        return `<div class="form-check">
                                <input type="checkbox" class="form-check-input check_row" ${checked} data-id="${row.id}">
                                </div>`;
                    },
                },
                {
                    targets: 2,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        return '<a target="_blank" href="' + data + '">'+data+'</a>';
                    },
                },
                {
                    targets: 7,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        return ' <a class="a_details_page_related" href="#" title="Ver detalles">Ver detalles</a>';
                    },
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol, a) {
                        // console.log('DATA O--> :'+ JSON.stringify(oData)  )
                        $('.a_details_page_related', nTd).click(function (e) {
                            dt_datailt_page_related(sData.url_page)
                            $('#modal_details_page_related').modal()
                            if (sData.substr(0, 1) === '#') {
                                $('.lbl_datilt_theme').text(sData.url_page)
                                sData = sData.substr(1, sData.length)
                                $('.btn_view_detailt_theme_tw').attr('href', 'https://twitter.com/hashtag/' + sData + '?src=tren')
                            } else {
                                $('.lbl_datilt_theme').text(sData)
                                $('.btn_view_detailt_theme_tw').attr('href', 'https://twitter.com/search?q=' + '"' + sData + '"' + '&src=tren')
                            }
                        });
                    }
                    
                },
            ],
        });

    dt2.on('click', '.check_row', function () {
        let status = $(this).is(':checked');

        if (status === false) $('#allCheck').prop('checked', false);
        else {
            let var_1 = true;
            let var_2 = true;

            $('#table_pendent_pages tbody tr').each(function () {
                let input = $(this).find("td").find("div").find("input");
                if (input.is(':checked') === false) var_2 = false
            });

            if (var_1 === var_2) $('#allCheck').prop('checked', true);
        }
    })

    dt2.on('click', '#allCheck', function () {
    let statud_check = $(this).is(':checked');
    console.log(statud_check)

    $('#table_pendent_pages tbody tr').each(function () {
        let input = $(this).find("td").find("div").find("input");
        if (statud_check === true) $(input).prop('checked', true);
        else $(input).prop('checked', false);
    });
});
})