let dt2 = $('#table_credentials');

$(document).ready(function () {

    get_list_facebook();

    $('#create-new').click(function () {
        $('#exampleModal').modal('toggle');
        $('#form_credential #btnregistrodata').text('Nuevo Registro');
        $('#form_credential #i_fcre_id').val('');
        $('#form_credential #v_fcre_name_application').val('');
        $('#form_credential #v_fcre_email').val('');
        $('#form_credential #v_fcre_app_id').val('');
        $('#form_credential #v_fcre_app_secret').val('');
        $('#form_credential #v_fcre_extend_token').val('');
        $('#form_credential #i_fcre_type').val(1);
    });


    $('#btnregistrodata').click(function () {
        let form = $('#form_credential');
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {
                dt2.ajax.url('/facebook/ajax_credential_list/').load();
            }
        });
    })
})

function get_list_facebook() {
    dt2.DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: '/facebook/ajax_credential_list/',
                type: 'GET'
            },
            language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},

            columns: [
                {data: "i_fcre_id"},
                {data: "v_fcre_name_application"},
                {data: "v_fcre_email"},
                {data: "v_fcre_app_id"},
                {data: "v_fcre_app_secret"},
                {data: "v_fcre_extend_token"},
                {data: "i_fcre_type"}
            ],
            columnDefs: [
                {
                    targets: 5,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (data.length > 20) {
                            return data.substring(0, 20) + '....';
                        }
                        return '';
                    },
                },
                {
                    targets: 6,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        switch (data) {
                            case 1:
                                return "1 Extraer datos de una fansPage";
                            case 2:
                                return "2 Extraer feeds de una fansPage";
                            case 3:
                                return "3 Extraer comentarios de un Feed";
                        }
                        return '';
                    },
                },
                {
                    targets: 7,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        return `<span class="text-success" data-toggle="tooltip" data-placement="left" 
                                title="Editar"></span> <a data-id="${data.i_fcre_id}" class="badge badge-info 
                                action_edit_credential"><i class="fa fa-edit"></i></a>&nbsp;<span class="text-alert" 
                                data-toggle="tooltip" data-placement="left" title="Eliminar"></span> 
                                <a data-id="${data.i_fcre_id}" class="badge badge-danger action_delete_credential" 
                                title="Eliminar"><i class="fa fa-trash"></i></a>&nbsp;`;
                    },
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol, a) {
                        let csr = $('input[name=csrfmiddlewaretoken]').val();
                        $(".action_delete_credential", nTd).click(function (e) {
                            var idCredential = $(this).attr("data-id")
                            //$('#exampleModal').modal('toggle');
                            $.ajax({
                                data: {
                                    csrfmiddlewaretoken: csr
                                },
                                type: 'GET',
                                url: '/facebook/delete_credential/' + idCredential,
                                success: function (data) {
                                    dt2.ajax.url('/facebook/ajax_credential_list/').load();
                                },
                                error: function () {
                                    alert('error del servidor o opcion no válida..!');
                                }
                            });
                        });
                        $(".action_edit_credential", nTd).click(function (e) {
                            $('#exampleModal').modal('toggle');
                            $('#form_credential #btnregistrodata').text('Actualizar')
                            let idCredential = $(this).attr("data-id")
                            $.ajax({
                                data: {
                                    csrfmiddlewaretoken: csr
                                },
                                type: 'get',
                                url: '/facebook/get_credential/' + idCredential,
                                success: function (data) {
                                    $('#form_credential #i_fcre_id').val(data.data.i_fcre_id);
                                    $('#form_credential #v_fcre_name_application').val(data.data.v_fcre_name_application);
                                    $('#form_credential #v_fcre_email').val(data.data.v_fcre_email);
                                    $('#form_credential #v_fcre_app_id').val(data.data.v_fcre_app_id);
                                    $('#form_credential #v_fcre_app_secret').val(data.data.v_fcre_app_secret);
                                    $('#form_credential #v_fcre_extend_token').val(data.data.v_fcre_extend_token);
                                    $('#form_credential #i_fcre_type').val(data.data.i_fcre_type);
                                },
                                error: function () {
                                    alert('error del servidor o opcion no válida..!');
                                }
                            });
                        });
                        // detect event checked
                        $(".check_status", nTd).click(function (e) {
                            status = $(this).is(':checked')
                            let id_theme = $(this).attr("data-id")
                            $.ajax({
                                data: {
                                    id_theme: id_theme,
                                    status: status,
                                    csrfmiddlewaretoken: csr
                                },
                                type: 'post',
                                url: '/influenciador/update-status-tw-user-ajax',
                                success: function (data) {
                                    let resultado = JSON.parse(data);
                                    if (resultado['estado']) {
                                        dt2.ajax.url('/api2/user-tracing/').load();
                                    } else {
                                        alert('error:' + resultado['error'])
                                    }
                                },
                                error: function () {
                                    alert('error del servidor o opcion no válida..!');
                                }
                            });
                        })
                    }
                },
            ],
        });
}