let dt2 = $('#table_pages');

$(document).ready(function () {

    dt2.DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: '/facebook/ajax_posts_list/',
                type: 'GET'
            },
            language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},

            columns: [
                {data: "id_page"},
                {data: "created_time"},
                {data: "date_register"},
                {data: "message"},
                {data: "permalink_url"},
                {data: "shares"},
                {data: "reaction_all"},
                {data: "cant_comment"},
                {data: "name_page"},
                {data: "link_page"}
            ],
            columnDefs: [
                {
                    targets: 4,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        return '<a target="_blank" href="' + data + '">Link</a>';
                    },
                },
            ],
        });
    $('#btnregistrodata').click(function () {
        var form = $('#form_admin_pages');
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {
                alert(response.response + ' : ' + response.message)
                dt2.ajax.url('/facebook/ajax_posts_list/').load();
                $('#fansPageAdminModal').modal('toggle');
            }
        });
    })
})