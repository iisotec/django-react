let dt2 = $('#table_pages')
let csr = $('input[name=csrfmiddlewaretoken]').val()
let url_status_seguimiento = dt2.attr('data-url-status');

$(document).ready(function () {
    $('#register_new_page').click(function () {
        $('#form_admin_pages')[0].reset();
        $('#fansPageAdminModal').modal('toggle');
        $('#form_admin_pages #btnregistrodata').text('Nuevo Registro');
        $('#form_admin_pages #url_fans_page').val('');
    })

    get_list_page();

    $('#btnregistrodata').click(function () {
        if ($('#form_admin_pages').valid()) {
            let form = $('#form_admin_pages');
            let dt1 = dt2.DataTable();
            $(".loader_insert_page_ajax").show();
            $.ajax({

                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    $(".loader_insert_page_ajax").hide();
                    alert(response.response + ' : ' + response.message)
                    dt1.ajax.url('/facebook/ajax_pages_list/').load();
                    $('#fansPageAdminModal').modal('toggle');
                }
            });
        }
    })

    $('#btn_update_facebook_page').on('click', function (e) {

        if ($('#form_edit_facebook_page').valid()) {
            $(".cargar_loader_ajax").show();
            $.ajax({
                data: {
                    id_page: $('#p_id_page').text(),
                    country: $("#s_country option:selected").val(),
                    csrfmiddlewaretoken: csr
                },
                type: 'post',
                url: '/facebook/update_facebook_page_ajax',
                success: function (data) {
                    let resultado = (data);
                    if (resultado['estado']) {
                        $(".cargar_loader_ajax").hide();
                        alert("Se ha actualizado correctamente...!")
                        $('#table_pages').DataTable().draw('page');
                        $('#FacebookPageModal').modal('toggle');

                    } else {
                        alert("Error de actualizacin..!" + resultado['error'])
                    }
                }
            });
        }
    });

    dt2.on('click', '.check_row', function () {
        let status = $(this).is(':checked');

        if (status === false) $('#allCheck').prop('checked', false);
        else {
            let var_1 = true;
            let var_2 = true;

            $('#table_pages tbody tr').each(function () {
                let input = $(this).find("td").find("div").find("input");
                if (input.is(':checked') === false) var_2 = false
            });

            if (var_1 === var_2) $('#allCheck').prop('checked', true);
        }
    })
});

function change_status_date(array_id_site_true, array_id_site_false) {
    let csr = $('input[name=csrfmiddlewaretoken]').val()

    array_id_site_false = array_id_site_false.length === 0 ? [] : array_id_site_false;
    array_id_site_true = array_id_site_true.length === 0 ? [] : array_id_site_true;

    $.ajax({
        data: {
            array_id_true: array_id_site_true,
            array_id_false: array_id_site_false,
            csrfmiddlewaretoken: csr
        },
        type: 'post',
        url: url_status_seguimiento,
        success: function (data) {
            console.log('data ==>> ', data)
            reload_table_for_filter();
            $("#select-option").val('')
        },
        error: function () {
            console.log('error del servidor o opcion no válida..!');
        }
    });
}

function reload_table_for_filter() {
    let dt1 = dt2.DataTable();
    let url = `/facebook/ajax_pages_list/`;
    dt1.ajax.url(url).load();
}

function get_list_page() {
    let vuelta = 1;
    dt2.DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: '/facebook/ajax_pages_list/',
                type: 'GET'
            },
            language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},

            columns: [
                {data: "status_manual_tracking"},
                {data: "id"},
                {data: "name"},
                {data: "link"},
                {data: "fan_count"},
                {data: "rating_count"},
                {data: "talking_about_count"},
                {data: "were_here_count"},
                {data: "username"},
                {data: "categoryName"},
                {data: "country"},
                {data: "location_city"}
            ],
            columnDefs: [
                {width: "2%", className: "text-center", targets: [0]},
                {
                    targets: 0,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (vuelta === 1) $('#allCheck').prop('checked', true);
                        let checked = data === true ? 'checked' : '';
                        let val_bool = data === true

                        if (val_bool === true) $('#allCheck').prop('checked', true);
                        else {
                            $('#allCheck').prop('checked', false);
                            vuelta = 0
                        }
                        return `<div class="form-check">
                                <input type="checkbox" class="form-check-input check_row" id="allCheck" ${checked} data-id="${row.id}">
                                </div>`;
                    },
                },
                {
                    targets: 4,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        return '<a target="_blank" href="' + data + '">' + data + '</a>';
                    },
                },
                {
                    targets: 12,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {

                        if (row.manually_validated_data) {
                            return `<span class="text-success" data-toggle="tooltip" data-placement="left" 
                                    title="Validado"><i class="fa fa-check-circle "></i></span> 
                                    <a data-id-page="${row.id}" class="badge badge-primary" data-toggle="tooltip" 
                                    data-placement="left" title="Editar"><i class="fa fa-edit"></i></a>`;
                        } else {
                            return `<span class="text-secondary" data-toggle="tooltip" data-placement="left" 
                                    title="Por validar" style="color:#b5b6b7!important">
                                    <i class="fa fa-circle "></i></span><a data-id-page="${row.id}" 
                                    class="badge badge-primary" data-toggle="tooltip" data-placement="left" 
                                    title="Editar"><i class="fa fa-edit"></i></a>`;
                        }
                    },
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol, a) {
                        $("a", nTd).click(function (e) {
                            $.ajax({
                                data: {
                                    id_post: $(this).attr("data-id-page"),
                                    csrfmiddlewaretoken: csr
                                },
                                type: 'post',
                                url: '/facebook/detail-data-facebook-page-ajax',
                                success: function (data) {
                                    let resultado = JSON.parse(data);
                                    if (resultado.estado) {
                                        if (resultado.data.country) {
                                            $("#s_country option[value=" + resultado.data.country + "]").prop('selected', true);
                                        } else {
                                            $('#s_country').prop('selectedIndex', 0);
                                        }

                                        $('#p_id_page').text(resultado.data.id);
                                        $('#p_name').text(resultado.data.name);
                                        $('#p_username').text(resultado.data.username);
                                        $('#p_validate').text(resultado.data.manually_validated_data);
                                        $('#p_category').text(resultado.data.categoryName);
                                        $('#FacebookPageModal').modal('toggle');
                                    } else {
                                        alert("Error al optener datos..!")
                                    }
                                }
                            });
                        });
                    }
                },
            ],
        });

    $('.container-fluid ').on('click', 'input[type="search"]', function () {
        vuelta = 1
    });

    $('#select-option').on('change', function () {
        let array_id_site_false = [];
        let array_id_site_true = [];

        Swal({
            title: '¿Estas seguro de cambiar el dato de la publicacion',
            text: "¡No podrás revertir esto!",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si!'
        }).then((result) => {
            if (result.value) {
                let status_id = false
                $('#table_pages tbody tr').each(function () {
                    let input = $(this).find("td").find("div").find("input");
                    let id_site = $(input).data('id')

                    if (input.is(':checked') === true) {
                        array_id_site_true.push(id_site);
                        status_id = true
                    } else {
                        array_id_site_false.push(id_site);
                        status_id = false
                    }
                });

                change_status_date(array_id_site_true, array_id_site_false)
            } else $("#select-option").val('')
        })
    })
}