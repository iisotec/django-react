let dt2 = $('#table_pages_instagram');

$(document).ready(function () {

    list_post_instragram();

    $('#btnregistrodata').click(function () {
        let form = $('#form_admin_pages');
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {
                alert(response.response + ' : ' + response.message)
                dt2.ajax.url('/facebook/ajax_posts_list/').load();
                $('#fansPageAdminModal').modal('toggle');
            }
        });
    })

    click_view_more('#table_pages_instagram');
});

function list_post_instragram() {
    dt2.DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: '/instagram/ajax_posts_list_instagram/',
                type: 'GET'
            },
            language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},

            columns: [
                {data: "id"},
                {data: "createRegister"},
                {data: "create"},
                {data: "createUpdate"},
                {data: "captionText"},
                {data: "url"},
                {data: "commentCount"},
                {data: "commentLikesEnabled"},
                {data: "imageVersions2"},
                {data: "likeCount"},
                {data: "is_video"},
                {data: "userFullName"},
                {data: "userUsername"},
                {data: "country"}
            ],
            columnDefs: [
                {
                    targets: 4,
                    render: function (data, type, row) {
                        return toggle_view_more(row['id'], data);
                    },
                },
                {
                    targets: 5,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        return `<a target="_blank" href="${data}">Enlace post</a>`;
                    },
                },
                {
                    targets: 8,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        return `<a target="_blank" href="${data}">Enlace imagen</a>`;
                    },
                },
            ],
        });
}