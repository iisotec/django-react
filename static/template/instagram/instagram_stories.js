let dt2 = $('#table_stories_instagram');

$(document).ready(function () {

    list_stories_instragram();

    $('#aaa').click(function () {
        let form = $('#form_add_history');
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {
                alert(response.response + ' : ' + response.message)
                dt2.ajax.url('/instagram/ajax_stories_list_instagram/').load();
                $('#modal_add_history').modal('toggle');
            }
        });
    })
    click_view_more(dt2);
});

// add user instagram
$('#register_new_history_instagram').click(function () {
    $('#modal_add_history').modal('toggle');
    // $('#form_add_user_instagram #screen_name').val('');
})

function list_stories_instragram() {
    domain = window.location.origin
    dt2.DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: '/instagram/ajax_stories_list_instagram/',
                type: 'GET'
            },
            language: {url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"},

            columns: [
                {data: "id"},
                {data: "date_create"},
                {data: "path_user_photo"},
                {data: "text_photo"},
                {data: "type_dowload"},
                {data: "user_follow"},
                {data: "user_photo"},
                {data: "video_src"},
                {data: "img_src"},
                {data: "name_user"},
                {data: "date_shared"},
                {data: "url_history"},
                {data: "path_user_video"}
            ],
            columnDefs: [
                {
                    targets: 0,
                    render: function (data, type, row) {
                        return toggle_view_more(row['id'], data);
                    },
                },
                {
                    targets: 2,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) { 
                        if (data.indexOf('chamec.com') > -1)
                        {
                          return `<a target="_blank" href="${data}">Enlace historia en chamec</a>`;
                        }
                        else{
                            return `<a target="_blank" href="${domain}${data}">Enlace historia en chamec</a>`;
                        }
                    
                    },
                },
                {
                    targets: 6,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (data)
                        {
                          return `<a target="_blank" href="${data}">Enlace foto usuario en instagram</a>`;
                        }
                        else{
                            return `Sin foto`;
                        }
                    
                    },
                },
                {
                    targets: 7,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (data)
                        {
                          return `<a target="_blank" href="${data}">Enlace video en instagram</a>`;
                        }
                        else{
                            return `Sin enlace`;
                        }
                    
                    },
                },
                {
                    targets: 8,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (data)
                        {
                          return `<a target="_blank" href="${data}">Enlace imagen en instagram</a>`;
                        }
                        else{
                            return `Sin enlace`;
                        }
                    
                    },
                },
                {
                    targets: 11,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (data)
                        {
                          return `<a target="_blank" href="${data}">url historia en instagram</a>`;
                        }
                        else{
                            return `Sin enlace`;
                        }
                    
                    },
                },
                {
                    targets: 12,
                    data: null,
                    searchable: false,
                    render: function (data, type, row) {
                        if (data) {
                            if (data.indexOf('chamec.com') > -1)
                            {
                              return `<a target="_blank" href="${data}">url video en chamec</a>`;
                            }
                            else{
                                return `<a target="_blank"${domain}${data}">url video en chamec</a>`;
                            }
                        }
                        else{
                            return `Sin video`;
                        }
                    
                    },
                },
            ],
        });
}