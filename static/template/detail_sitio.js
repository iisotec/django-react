let form = $('#form');
let url_insert_sitio = form.attr('data-insert');
let url_zone_time = form.attr('data-list-zone-time');
let url_list_site = form.attr('data-list-site');
let data_get_view = form.attr('data-get-view');
let select_country = $('#country');
let select_submit = $('#submit');
let csr = $('input[name=csrfmiddlewaretoken]').val();
let loading = $('#loading');
let check_url_preview = $('#check_url_preview');
$(document).ready(function () {

    $('#div_date_rss').hide();

    $('#website_type_scraping').on('change', function () {
        if ($(this).val() === 'rss') {
            $('#div_date_rss').show();
        }else{
            $('#div_date_rss').hide();
        }
    });

    select_country.select2();
    let id_type_zone = $('#id_type_zone').attr('data-number');
    id_type_zone = id_type_zone.replace(',', '.');
    $('#time_zone').val(id_type_zone);
    form.submit(function (e) {
        e.preventDefault();

        toggleLoader(true);

        $.ajax({
            type: "POST",
            url: url_insert_sitio,
            data: form.serialize(),
            success: function (data) {
                cambio_boolean(true);
                window.location.replace(url_list_site)
            },
            error: function (e) {
                cambio_boolean(false);
                toggleLoader(false);
                alert(e['responseText']);
            }
        });
    })

    select_country.on('change', function () {
        let type_select_time = $('#time_zone');
        if ($(this).val()) {
            $.ajax({
                type: 'POST',
                data: {
                    'country_code': $(this).val(),
                    'csrfmiddlewaretoken': csr
                },
                url: url_zone_time,
                success: function (data) {
                    let objetos = JSON.parse(data);
                    type_select_time.val(objetos['data']);
                },
                error: function () {
                    alert('error del servidor o opcion no válida..!');
                }
            });
        }
    });

    loading.on('click', function () {
        if ($("#form_url_preview").valid()) {
            toggleLoader(true);
            get_site_view();
        }
    })

    check_url_preview.on('click', function(){
        // $('.myCheckbox').prop('checked', true);
        if ($(this).is(':checked')) {
            $("#preview_url").prop('disabled', true);
             $('#preview_url').val('');
            $("#preview_url").prop('required',false);
        }
        else{
            $("#preview_url").prop('required',true);
            $("#preview_url").prop('disabled', false);
        }
    });

});

function get_site_view() {
    $.ajax({
        type: "POST",
        url: data_get_view,
        data: {
            site_domain: $('#site_domain').val(),
            country: select_country.val(),
            time_zone: $('#time_zone').val(),
            csrfmiddlewaretoken: csr,
            url_loader: $("#preview_url").val()
        },
        success: function (data) {
            toggleLoader(false);
            loading.html('Vista previa')

            let data_query = jQuery.parseJSON(data);
            let data_response = data_query['response']['response'];
            let summary = $('#summary');
            let text = $('#text');
            let meta_description = $('#meta_description');

            if (data_query['response']['estado'] === false) {
                loading.html('Error no carga vista');
                $('#modalButon').modal('toggle');
            } else {
                $('#url').html(create_etiqueta_a_href(data_response['url']));
                $('#url_origin').html(create_etiqueta_a_href(data_response['url_origin']));
                $('#netloc').html(data_response['netloc']);
                $('#top_image').html(create_etiqueta_a_href(data_response['top_image']));
                $('#date_create').html(data_response['date_create']);
                $('#date_publication').html(data_response['date_publication']);
                $('#title').html(data_response['title_text']);
                summary.html(toggle_view_more('summary', data_response['summary']));
                text.html(toggle_view_more('text', data_response['text']));
                meta_description.html(toggle_view_more('meta_description', data_response['meta_description']));
            }
        },
        error: function (e) {
            toggleLoader(false);
            loading.html('Error no carga vista');
            $('#modalButon').modal('toggle');
        }
    });

    click_view_more('.modal-body');
}

function create_etiqueta_a_href(text) {
    return `<a href="${text}" target="_blank">${text}</a>`
}

function cambio_boolean(bool) {
    select_submit.prop('valid', bool);
    select_submit.prop('disabled', bool);
    $('.form-control').prop('disabled', bool);
}

function toggleLoader(show) {
    let select_pre = $('.se-pre-con');
    let select_content = $('.site-content');
    if (show === true) {
        select_pre.show();
        select_content.fadeOut();
    } else {
        select_content.show();
        select_pre.fadeOut();
    }
}