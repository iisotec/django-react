let form = $('#form');
let table_excel = $('#table_excel');

$(document).ready(function () {
    funcionalidad_de_upload_file()

    form.submit(function (e) {
        e.preventDefault();
        toggleLoader(true);
        table_excel.DataTable().destroy();
        let formData = new FormData(this);

        $.ajax({
            method: 'POST',
            type: 'POST',
            headers: {
                'X-CSRFToken': CSRF_TOKEN,
            },
            url: form.data('url'),
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {
                let data_result = JSON.parse(result);
                table_excel.DataTable({
                    "data": data_result.response,
                    "columns": [
                        {
                            "data": "url_final",
                            render: function (data, type, row) {
                                let data_url = '';
                                if (data === null || data === undefined) data_url = '';
                                else data_url = data;
                                return data_url
                            }
                        },
                        {
                            "data": "url",
                            render: function (data, type, row) {
                                let data_url = '';
                                if (data === null || data === undefined) data_url = '';
                                else data_url = data;
                                return data_url
                            }
                        },
                        {
                            "data": "netloc",
                            render: function (data, type, row) {
                                let data_url = '';
                                if (data === null || data === undefined) data_url = '';
                                else data_url = data;
                                return data_url
                            }
                        },
                        {
                            "data": "observacion",
                            render: function (data, type, row) {
                                let data_url = '';
                                if (data === null || data === undefined) data_url = '';
                                else data_url = data;
                                return data_url
                            }
                        },
                        {
                            "data": "country",
                            render: function (data, type, row) {
                                return `<span class="text-success" data-toggle="tooltip" data-placement="left" 
                                        title="Redirigir"></span><a href="#"  title ="Redirigir" 
                                        data-url-final="${row['url_final']}" data-country="${row['country']}"
                                        data-date-create="${row['date_create']}" data-date-publication="${row['date_publication']}"
                                        data-netloc="${row['netloc']}" data-observacion="${row['observacion']}"
                                        data-summary="${row['summary']}" data-text="${row['text']}"
                                        data-title-text="${row['title_text']}" data-url="${row['url']}"
                                        data-url-origin="${row['url_origin']}" 
                                        data-menu-obs="${row['contiene_menu_obs']}"
                                        class="view_modal badge badge-info" data-toggle="modal" 
                                        data-target="#modalButon"><i class="fa fa-edit"></i></a>`;
                            }
                        },
                    ]
                });
                toggleLoader(false);
            },
            error: function (error) {
                console.log('error: ', error);
                toggleLoader(false);
            }
        });
    });

    click_more_view();
    set_data_modal();

});

function set_data_modal() {
    $('#table_excel').on('click', '.view_modal', function () {
        $('#url_final').html($(this).attr('data-url-final'));
        $('#url').html($(this).attr('data-url'));
        $('#netloc').html($(this).attr('data-netloc'));
        $('#title_text').html($(this).attr('data-title-text'));
        $('#summary').html($(this).attr('data-summary'));
        $('#text').html($(this).attr('data-text'));
        $('#country').html($(this).attr('data-country'));
        $('#date_create').html($(this).attr('data-date-create'));
        $('#date_publication').html($(this).attr('data-date-publication'));
        $('#menu_obs').html($(this).attr('data-menu-obs'));
    })
}

function click_more_view() {
    let moretext = "Seguir leyendo >";
    let lesstext = "Mostrar menos";
    $('#table_excel').on('click', '.morelink', function () {
        console.log(this)
        if ($(this).hasClass('less')) {
            $(this).removeClass('less');
            $(this).html(moretext);
            $('.abstract').removeClass('hidden');
        } else {
            $(this).addClass('less');
            $(this).html(lesstext);
            $('.abstract').addClass('hidden');
        }
        $(this).parent().prev().slideToggle('fast');
        $(this).prev().slideToggle('fast');
        return false;
    });
}

function funcionalidad_de_upload_file() {
    $(".input-file").before(
        function () {
            if (!$(this).prev().hasClass('input-ghost')) {
                let element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                element.attr("name", $(this).attr("name"));
                element.change(function () {
                    element.next(element).find('input').val((element.val()).split('\\').pop());
                });
                $(this).find("button.btn-choose").click(function () {
                    element.click();
                });
                $(this).find("button.btn-reset").click(function () {
                    element.val(null);
                    $(this).parents(".input-file").find('input').val('');
                });
                $(this).find('input').css("cursor", "pointer");
                $(this).find('input').mousedown(function () {
                    $(this).parents('.input-file').prev().click();
                    return false;
                });
                return element;
            }
        }
    );
}

function verificar_extencion_html(html_extencion) {
    let showChar = 50;
    let ellipsestext = "...";
    let moretext = "Seguir leyendo >";

    if (html_extencion.length > showChar) {
        let c = html_extencion.substr(0, showChar);
        return `<div class="abstract">${c}${ellipsestext}</div>
                    <div class="morecontent">${html_extencion}</div>
                    <p><span class="morelink">${moretext}</span></p>`;
    }
}

function toggleLoader(show) {
    if (show === true) {
        $(".se-pre-con").show();
        $(".site-content").fadeOut();
    } else {
        $(".site-content").show();
        $(".se-pre-con").fadeOut();

    }
}